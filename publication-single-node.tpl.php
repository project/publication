<?php
// $Id$
// @todo if we can get another template to be used then move this to a preprocess template 
$title = $node->title;
$url = url($node->path, array('absolute' => TRUE));
$date = format_date($node->created, 'medium');

if ($email_format == 'text') { 

  $output = $title."\n\n";
  $output .= $date."\n\n";
  $output .= $node->teaser ."\n\n"; 
  $output .= "-----------------------\n\n";
  print html_entity_decode($output);

} else {
	
  $output = $title."\n\n";
  $output .= $date."\n\n";
  $output .= $node->teaser ."\n\n"; 
  $output .= "-----------------------\n\n";
  print html_entity_decode($output);

?>
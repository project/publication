********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: Publication Module
Author: Robert Castelo
Drupal: 6.x
********************************************************************
DESCRIPTION:

Defines  the  content  of  a publication. The  publication  can  be  used  for  most  purposes,  
and includes the following information:

Type: 
Type  of  publication - ties  publication  to the module using publication.module.

Tiltle: 
Name of publication.

Description: 
Description of publication.

Templates:
Which template(s) to use for publication.


This module provides an API, don't install it unless required by another module.



********************************************************************
INSTALLATION:

Note: It is assumed that you have Drupal up and running.  Be sure to
check the Drupal web site if you need assistance.  If you run into
problems, you should always read the INSTALL.txt that comes with the
Drupal package and read the online documentation.

1. Place the entire publication directory into your Drupal modules/
   directory.

2. Enable the publication module by navigating to:

     administer > build > modules
     
  Click the 'Save configuration' button at the bottom to commit your
  changes.


********************************************************************
AUTHOR CONTACT

- Report Bugs/Request Features:
   http://drupal.org/project/publication
   
- Comission New Features:
   http://www.codepositive.com/contact
   
- Want To Say Thank You:
   http://www.amazon.com/gp/registry/O6JKRQEQ774F




********************************************************************
ACKNOWLEDGEMENT

Developed by Robert Castelo for Code Positive <http://www.codepositive.com>


